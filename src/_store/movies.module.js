import { userService } from '../_services';

const state = {
    all: {},
    single:{}
};

const actions = {
    getAll({ commit }, { searchString,year }) {
        commit('getAllRequest');

        userService.getAll(searchString,year)
            .then(
                movies => commit('getAllSuccess', movies),
                error => commit('getAllFailure', error)
            );
    },

    getSingle({ commit }, { imdbID }) {

        commit('getSingleRequest');

        userService.getSingle(imdbID)
            .then(
                movie => { commit('getSingleSuccess', movie); },
                error => commit('getSingleFailure', error)
            );
    },

};

const mutations = {
    getAllRequest(state) {
        state.all = { loading: true };
    },
    getAllSuccess(state, movies) {
        state.all = { loading: false,items: movies.Search };
    },
    getAllFailure(state, error) {
        state.all = { error };
    },

    getSingleRequest(state) {
        state.single = { loading: true };
    },
    getSingleSuccess(state, movie) {
        state.single = { loading: false,item: movie };
    },
    getSingleFailure(state, error) {
        state.single = { error };
    },
};

export const movies = {
    namespaced: true,
    state,
    actions,
    mutations
};
