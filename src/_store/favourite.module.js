import { userService } from '../_services';

const state = {
    add: {},
    get: {},
};

const actions = {
    addFavourite({ commit }, { Title,Year,imdbID,Type }) {
        console.log(Title,Year,imdbID,Type)
        commit('addFavouriteRequest');

        userService.addFavourite(Title,Year,imdbID,Type)
            .then(
                commit('addFavouriteSuccess'),
                error => commit('addFavouriteFailure', error)
            );
    },

    getFavourite({ commit }) {
        commit('getFavouriteRequest');

        userService.getFavourite()
            .then(
                movies => commit('getFavouriteSuccess', movies),
                error => commit('getFavouriteFailure', error)
            );
    },


};

const mutations = {
    addFavouriteRequest(state) {
        state.add = { loading: true,added:false };
    },
    addFavouriteSuccess(state) {
        state.add = { loading: false};
    },
    addFavouriteFailure(state, error) {
        state.add = { error };
    },


    getFavouriteRequest(state) {
        state.get = { loading: true,added:false };
    },
    getFavouriteSuccess(state,movies) {
        movies = JSON.parse(movies.favouriteMovies)
        console.log(movies)
        state.get = { loading: false,items: movies};
    },
    getFavouriteFailure(state, error) {
        state.get = { error };
    },


   
};

export const favourite = {
    namespaced: true,
    state,
    actions,
    mutations
};
