import { authHeader } from '../_helpers';

export const userService = {
    login,
    logout,
    register,
    getAll,
    getSingle,
    addFavourite,
    getFavourite
};
const serverURL = 'http://127.0.0.1:8000'
function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 'email': email, 'password': password })
    };

    return fetch(serverURL+`/CoreAuth/login/`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    let user = JSON.parse(localStorage.getItem('user'));
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify({ 'email': user.email })
    };
    localStorage.removeItem('user');

    return fetch(serverURL+`/CoreAuth/logout/`, requestOptions).then(handleResponse);


}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 'username': user.username, 'email': user.email, 'password': user.password })
    };

    return fetch(serverURL+`/CoreAuth/create/`, requestOptions).then(handleResponse);
}

function getAll(searchString,year) {
    const requestOptions = {
        method: 'GET',
    };
    if(year===""){
        return fetch(`https://www.omdbapi.com/?apikey=e1e04a26&s=`+searchString, requestOptions).then(handleResponse);
    }
    else{
        return fetch(`https://www.omdbapi.com/?apikey=e1e04a26&s=`+searchString+`&y=`+year, requestOptions).then(handleResponse);
    }

    
}

function getSingle(imdbID) {
    const requestOptions = {
        method: 'GET',
    };

    return fetch(`https://www.omdbapi.com/?apikey=e1e04a26&i=`+imdbID, requestOptions).then(handleResponse);
}


function addFavourite(Title,Year,imdbID,Type) {
    let user = JSON.parse(localStorage.getItem('user'));
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify({ 'email': user.email, 'Title':Title,'Year':Year,'imdbID': imdbID,'Type':Type })
    };

    return fetch(serverURL+`/CoreSearch/addFavourite/`, requestOptions).then(handleResponse);
}
function getFavourite() {
    let user = JSON.parse(localStorage.getItem('user'));
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify({ 'email': user.email })
    };

    return fetch(serverURL+`/CoreSearch/getAllFavourites/`, requestOptions).then(handleResponse);
}
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        
        if(data.Response==="False"){
            const error = (data && data.Error) || response.statusText;
            
            return Promise.reject(error);
        }

        return data;
    });
}