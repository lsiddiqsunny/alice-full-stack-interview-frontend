import Vue from 'vue';
import VeeValidate from 'vee-validate';

import { store } from './_store';
import { router } from './_helpers';
import App from './app/App';
import vuetify from './plugins/vuetify' // path to vuetify export
import vmodal from 'vue-js-modal'
Vue.use(VeeValidate);
Vue.use(vmodal, { dialog: true })



new Vue({
    el: '#app',
    router,
    store,
    vuetify,
    render: h => h(App)
});